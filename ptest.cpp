//
// Created by david on 2/5/16.
//

#include "ptest.h"

#include <iostream>

ptest::ptest()
{
    std::cout<<"Creating ptest obj.." <<std::endl;

    this->imageIn = this->imgInPI.GetImage();

    this->imageOut = OutputImageType::New();
    this->imgOutPI.SetOutputImage(this->imageOut);
}

ptest::~ptest()
{
    std::cout<<"Deleting ptest obj.." <<std::endl;
}

void ptest::doSomething()
{
    std::cout<<"Doing something.." <<std::endl;
}