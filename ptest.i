%module ptest
%{
#include "ptest.h"
%}

%include "pImg.i"

class ptest {
public:
    typedef unsigned short InPixelType;
    typedef float OutPixelType;

    ptest();
    ~ptest();

    pImgIn<InPixelType,2> imgInPI;
    pImgOut<OutPixelType,2> imgOutPI;

    void doSomething();
};