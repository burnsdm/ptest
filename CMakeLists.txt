cmake_minimum_required(VERSION 3.3)
project(pimg_test)

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(CMAKE_SWIG_OUTDIR ${CMAKE_HOME_DIRECTORY}/swig)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_HOME_DIRECTORY}/swig)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_HOME_DIRECTORY}/bin)

# VTK
find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

# ITK
set(ITK_DIR "/home/david/Code/Insight/build-pic")
find_package(ITK REQUIRED)
include(${ITK_USE_FILE})

# SWIG
find_package(SWIG REQUIRED)
include(${SWIG_USE_FILE})
set(CMAKE_SWIG_FLAGS "")

# Python
find_package(PythonLibs)
INCLUDE_DIRECTORIES(${PYTHON_INCLUDE_PATH})

# pImg
SET(PIMG_DIR "/home/david/Code/pImg/")

# local source files
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR} ${PIMG_DIR})
LINK_DIRECTORIES(${PIMG_DIR}/lib)

SET_SOURCE_FILES_PROPERTIES(ptest.i PROPERTIES CPLUSPLUS ON)
SET_SOURCE_FILES_PROPERTIES(ptest.i PROPERTIES SWIG_FLAGS "-includeall")

SWIG_ADD_MODULE(ptest python ptest.i ptest.h ptest.cpp)
SWIG_LINK_LIBRARIES(ptest ${PYTHON_LIBRARIES} ${VTK_LIBRARIES} ${ITK_LIBRARIES} pImg)

