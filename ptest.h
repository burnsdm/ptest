//
// Created by david on 2/5/16.
//



#ifndef PIMG_TEST_PTEST_H
#define PIMG_TEST_PTEST_H

#include <itkImage.h>

#include "pImgIn.h"
#include "pImgOut.h"

class ptest {
public:
    typedef unsigned short InPixelType;
    typedef float OutPixelType;

    ptest();
    ~ptest();

    pImgIn<InPixelType,2> imgInPI;
    pImgOut<OutPixelType,2> imgOutPI;

    void doSomething();

protected:
    typedef itk::Image<InPixelType,2> InputImageType;
    typedef itk::Image<OutPixelType,2> OutputImageType;
    typename InputImageType::Pointer imageIn;
    typename OutputImageType::Pointer imageOut;

};


#endif //PIMG_TEST_PTEST_H
